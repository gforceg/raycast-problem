extends Area2D


var ray: RayCast2D

func _ready():
	ray = get_node("RayCast2D")

func _input(i: InputEvent):
	print(self.name, ' ray collision: ', ray.is_colliding())
